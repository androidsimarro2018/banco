package com.simarro.practicas.banco_tomas_tortosa.adaptador;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.simarro.practicas.banco_tomas_tortosa.R;
import com.simarro.practicas.banco_tomas_tortosa.pojo.Cajeros;

import java.util.List;

public class adaptadorCajeros<T> extends ArrayAdapter<T> {

    public adaptadorCajeros(Context context, List<T> objects) {
        super(context, 0, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View listItemView = convertView;

        if (null == convertView) {

            listItemView = inflater.inflate(R.layout.activity_cajeros_detalle,parent,false);
        }

        TextView direccion = listItemView.findViewById(R.id.txtDireccion);
        TextView latitud = listItemView.findViewById(R.id.txtLatitud);
        TextView longitud= listItemView.findViewById((R.id.txtLongitud));
        TextView zoom = listItemView.findViewById(R.id.txtZoom);

        Cajeros c = (Cajeros)getItem(position);

        direccion.setText(c.getDireccion());
        latitud.setText(String.valueOf(c.getLatitude()));
        longitud.setText(String.valueOf(c.getLongitude()));
        zoom.setText(String.valueOf(c.getZoom()));

        return listItemView;
    }
}
