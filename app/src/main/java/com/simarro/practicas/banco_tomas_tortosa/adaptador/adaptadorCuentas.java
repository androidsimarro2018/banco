package com.simarro.practicas.banco_tomas_tortosa.adaptador;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.simarro.practicas.banco_tomas_tortosa.R;
import com.simarro.practicas.banco_tomas_tortosa.pojo.Cliente;
import com.simarro.practicas.banco_tomas_tortosa.pojo.Cuenta;

import java.util.ArrayList;

public class adaptadorCuentas extends ArrayAdapter<Cuenta> {

    Activity context;
    ArrayList<Cuenta> listaCuentas;

    public adaptadorCuentas(Fragment context, ArrayList<Cuenta> listaCuentas){

        super(context.getActivity(), R.layout.layout_elemento_lista, listaCuentas);
        this.context=context.getActivity();
        this.listaCuentas = listaCuentas;

    }

    public View getView(int position, View ConvertView, ViewGroup parent){

        LayoutInflater inflater = (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View item = ConvertView;

        if(null==ConvertView){
            item = inflater.inflate(R.layout.layout_elemento_lista, parent,false);
        }
        Cuenta cuenta = getItem(position);
        TextView lblCuenta = item.findViewById(R.id.txtCuenta);
        lblCuenta.setText(cuenta.getNumeroCuenta());

        return item;

    }

}
