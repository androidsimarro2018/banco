package com.simarro.practicas.banco_tomas_tortosa.adaptador;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.simarro.practicas.banco_tomas_tortosa.R;
import com.simarro.practicas.banco_tomas_tortosa.pojo.Cuenta;
import com.simarro.practicas.banco_tomas_tortosa.pojo.Movimiento;

import java.util.ArrayList;

public class adaptadorMovimientos extends ArrayAdapter<Movimiento> {

    Activity context;
    ArrayList<Movimiento> listaMovimientos;

    public adaptadorMovimientos(Fragment context, ArrayList<Movimiento> listaMovimientos){

        super(context.getActivity(), R.layout.layout_elemento_lista_movimiento, listaMovimientos);
        this.context=context.getActivity();
        this.listaMovimientos = listaMovimientos;

    }

    public View getView(int position, View ConvertView, ViewGroup parent){

        LayoutInflater inflater = (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View item = ConvertView;

        if(null==ConvertView){
            item = inflater.inflate(R.layout.layout_elemento_lista_movimiento, parent,false);
        }

        Movimiento movimiento = getItem(position);
        TextView lblMovimiento = item.findViewById(R.id.txtMovimiento);
        lblMovimiento.setText(movimiento.getDescripcion());

        return item;

    }

}
