package com.simarro.practicas.banco_tomas_tortosa.controller;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.simarro.practicas.banco_tomas_tortosa.R;
import com.simarro.practicas.banco_tomas_tortosa.pojo.Cajeros;
import com.simarro.practicas.banco_tomas_tortosa.pojo.Cliente;

public class AppActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView txtDni;
    private ImageButton btnPosicionGlobal;
    private ImageButton btnTransferencia;
    private ImageButton btnIngresos;
    private ImageButton btnPromociones;
    private ImageButton btnCajeros;
    private ImageButton btnContrasenya;
    private Button btnCerrarSesion;
    private TextView txtUsuario;
    private Cliente c;

    private String dni;
    private String contrasenya;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_app);

        Bundle parametros = this.getIntent().getExtras();

        if (parametros != null) {

            c = (Cliente) this.getIntent().getExtras().get("CLIENTE");

            txtUsuario = findViewById(R.id.txtUsuario);
            txtUsuario.setText("Bienvenido: " + c.getNombre());

        }

        // Estas dos declaraciones sirven para saber el dni y usuario para que si presionamos cambiar contrasenya pasarle estos datos para verificar los datos.
        dni = String.valueOf(c.getId());
        contrasenya = c.getClaveSeguridad();

        btnPosicionGlobal = findViewById(R.id.imgBtnPosicion);
        btnIngresos = findViewById(R.id.imgBtnIngresos);
        btnTransferencia = findViewById(R.id.imgBtnTransferencias);
        btnPromociones = findViewById(R.id.imgBtnPromociones);
        btnContrasenya = findViewById(R.id.imgBtnContrasenya);
        btnCajeros = findViewById(R.id.imgBtnCajeros);
        btnCerrarSesion = findViewById(R.id.btnCerrarSesion);

        btnPosicionGlobal.setOnClickListener(this);
        btnIngresos.setOnClickListener(this);
        btnTransferencia.setOnClickListener(this);
        btnPromociones.setOnClickListener(this);
        btnContrasenya.setOnClickListener(this);
        btnCajeros.setOnClickListener(this);
        btnCerrarSesion.setOnClickListener(this);

    }

    public boolean onCreateOptionsMenu(Menu menu) {
// Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        Intent intent = new Intent(AppActivity.this, Construccion.class);

        switch (item.getItemId()) {
            case R.id.posicionGlobal:
                Intent intentPosicion = new Intent(AppActivity.this, Posicion_Global_Cuentas.class);
                Bundle b = new Bundle();
                b.putSerializable("CLIENTE", c);
                intentPosicion.putExtras(b);
                startActivity(intentPosicion);
                return true;
            case R.id.ingresos:
                startActivity(intent);
                return true;
            case R.id.transferencias:
                Intent intentTransferencia = new Intent(AppActivity.this, Transferencia.class);
                intentTransferencia.putExtra("CLIENTE",c);
                startActivity(intentTransferencia);
                return true;
            case R.id.cambiarContrasenya:
                Intent intentContrasenya = new Intent(AppActivity.this, Cambio_Contrasenya.class);
                intentContrasenya.putExtra("CLIENTE",c);
                startActivity(intentContrasenya);
                return true;
            case R.id.promociones:
                startActivity(intent);
                return true;
            case R.id.cajerosCercanos:

                Intent intentCajeros = new Intent(this, Cajeros.class);
                startActivity(intentCajeros);

                return true;
            case R.id.configuration:
                Intent intentConfiguration = new Intent(AppActivity.this, Configuration.class);
                startActivity(intentConfiguration);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(View view) {

        Intent intent = new Intent(AppActivity.this, Construccion.class);

        switch (view.getId()) {

            case R.id.imgBtnPosicion:

                Intent intentPosicion = new Intent(AppActivity.this, Posicion_Global_Cuentas.class);
                Bundle b = new Bundle();
                b.putSerializable("CLIENTE", c);
                intentPosicion.putExtras(b);
                startActivity(intentPosicion);

                break;

            case R.id.imgBtnIngresos:

                startActivity(intent);

                break;

            case R.id.imgBtnTransferencias:

                Intent intentTransferencia = new Intent(AppActivity.this, Transferencia.class);
                intentTransferencia.putExtra("CLIENTE",c);
                startActivity(intentTransferencia);

                break;

            case R.id.imgBtnContrasenya: ///

                Intent intentContrasenya = new Intent(AppActivity.this, Cambio_Contrasenya.class);
                intentContrasenya.putExtra("CLIENTE",c);
                startActivity(intentContrasenya);

                break;

            case R.id.imgBtnPromociones:

                startActivity(intent);

                break;

            case R.id.imgBtnCajeros:

                Intent intentCajeros = new Intent(this, CajerosActivity.class);
                startActivity(intentCajeros);

                break;

            case R.id.btnCerrarSesion:

                Intent in = new Intent(AppActivity.this, MainActivity.class);
                startActivity(in);

        }

    }
}
