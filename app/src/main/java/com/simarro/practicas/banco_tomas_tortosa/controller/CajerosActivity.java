package com.simarro.practicas.banco_tomas_tortosa.controller;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import com.simarro.practicas.banco_tomas_tortosa.R;
import com.simarro.practicas.banco_tomas_tortosa.adaptador.adaptadorCajeros;
import com.simarro.practicas.banco_tomas_tortosa.dao.CajerosDAO;
import com.simarro.practicas.banco_tomas_tortosa.pojo.Cajeros;

import java.util.ArrayList;

public class CajerosActivity extends AppCompatActivity {

    private ListView listViewCajeros;
    private adaptadorCajeros<Cajeros> adaptadorCajeros;
    private ArrayList<Cajeros> listaCajeros = new ArrayList<Cajeros>();
    private CajerosDAO cajerosDAO;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cajeros);

        listViewCajeros = findViewById(R.id.lvCajeros);

        //RECUPERAMOS LOS DATOS DE LOS CAJEROS
        cajerosDAO = new CajerosDAO();

        listaCajeros = cajerosDAO.getAll();

        adaptadorCajeros = new adaptadorCajeros<Cajeros>(this, listaCajeros);

        listViewCajeros.setAdapter(adaptadorCajeros);

    }
}
