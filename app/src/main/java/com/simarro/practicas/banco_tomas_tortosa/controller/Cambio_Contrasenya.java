package com.simarro.practicas.banco_tomas_tortosa.controller;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.simarro.practicas.banco_tomas_tortosa.R;
import com.simarro.practicas.banco_tomas_tortosa.bd.MiBancoOperacional;
import com.simarro.practicas.banco_tomas_tortosa.pojo.Cliente;

public class Cambio_Contrasenya extends AppCompatActivity {

    private String dni;
    private String contrasenya;
    private TextView contrasenyaVieja;
    private TextView contrasenyaNueva1;
    private TextView contrasenyaNueva2;

    private int resultado;
    private Cliente c;
    private MiBancoOperacional mbo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cambio_contrasenya);

        Bundle parametros = this.getIntent().getExtras();

        if (parametros != null) {

            c = (Cliente) this.getIntent().getExtras().get("CLIENTE");

        }

        mbo = MiBancoOperacional.getInstance(this);

        dni = String.valueOf(c.getId());
        contrasenya = c.getClaveSeguridad();

        contrasenyaVieja = findViewById(R.id.txtViejaContrasenya);
        contrasenyaNueva1 = findViewById(R.id.txtNuevaContrasenya);
        contrasenyaNueva2 = findViewById(R.id.txtNuevaContrasenyaRepetir);

    }

    public boolean onCreateOptionsMenu(Menu menu) {
// Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        Intent intent = new Intent(this, Construccion.class);

        switch (item.getItemId()) {
            case R.id.posicionGlobal:
                Intent intentPosicion = new Intent(this, Posicion_Global_Cuentas.class);
                Bundle b = new Bundle();
                b.putSerializable("cli", c);
                intentPosicion.putExtras(b);
                startActivity(intentPosicion);
                return true;
            case R.id.ingresos:
                startActivity(intent);
                return true;
            case R.id.transferencias:
                Intent intentTransferencia = new Intent(this, Transferencia.class);
                startActivity(intentTransferencia);
                return true;
            case R.id.cambiarContrasenya:
                Intent intentContrasenya = new Intent(this, Cambio_Contrasenya.class);
                intentContrasenya.putExtra("CLIENTE",c);
                startActivity(intentContrasenya);
                return true;
            case R.id.promociones:
                startActivity(intent);
                return true;
            case R.id.cajerosCercanos:
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void Cambiar(View v) {

        if (contrasenya.equals(contrasenyaVieja.getText().toString())) { // SI LA CONTRASEÑA VIEJA INTRODUCIDA ES LA CORRECTA

            if (contrasenyaNueva1.getText().toString().equals(contrasenyaNueva2.getText().toString())) { // SI LAS DOS CONTRASEÑAS NUEVAS SON IGUALES

                //c = new Cliente(1, dni, null, null, contrasenyaNueva1.getText().toString(), null );

                c.setClaveSeguridad(contrasenyaNueva1.getText().toString());

                resultado = mbo.changePassword(c);


                if (resultado == 0) {
                    Toast.makeText(this, "ERROR!! NO SE HA PODIDO CAMBIAR LA CONTRASENYA", Toast.LENGTH_LONG).show();

                } else {
                    Toast.makeText(this, c.getClaveSeguridad(), Toast.LENGTH_LONG).show();

                }

            } else { // DOS CONTRASEÑAS NUEVAS NO SON IGUALES

                Toast.makeText(this, "ERROR!! LAS CONTRASEÑAS NUEVAS NO SON IGUALES", Toast.LENGTH_LONG).show();

            }

        } else { // CONTRASEÑA VIEJA NO ES LA MISMA QUE YA EXISTE

            Toast.makeText(this, "ERROR!! LA CONTRASEÑA VIEJA NO COINCIDE CON LA ACTUAL", Toast.LENGTH_LONG).show();

        }

        Intent intent = new Intent(Cambio_Contrasenya.this, AppActivity.class);
        intent.putExtra("CLIENTE", c);
        startActivity(intent);

    }
}
