package com.simarro.practicas.banco_tomas_tortosa.controller;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.simarro.practicas.banco_tomas_tortosa.R;
import com.simarro.practicas.banco_tomas_tortosa.bd.MiBancoOperacional;
import com.simarro.practicas.banco_tomas_tortosa.pojo.Cliente;
import com.simarro.practicas.banco_tomas_tortosa.pojo.Cuenta;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Button btnAcceder;
    private EditText txtDni;
    private EditText txtPassword;
    private String textoDni;
    private Cliente c;
    private MiBancoOperacional mbo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

         mbo = MiBancoOperacional.getInstance(this);

        //OBTENEMOS LA REFERENCIA A LOS CONTROLES DE LA INTERFAZ

        txtDni = findViewById(R.id.txtDni);
        txtPassword = findViewById(R.id.txtPassword);
        btnAcceder = findViewById(R.id.btnEntrar);

        btnAcceder.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        //c = new Cliente(0, txtDni.getText().toString(), null, null, txtPassword.getText().toString(), null );
        c = new Cliente(0, "11111111A", null, null, "1234", null );

        c = mbo.login(c);
        c.setListaCuentas(mbo.getCuentas(c));

        for (Cuenta cuenta: c.getListaCuentas()){
            cuenta.setListaMovimientos( mbo.getMovimientos(cuenta));
        }

        if(c==null){

            Toast.makeText(this, "Usuario y/o Contraseña Erroneos", Toast.LENGTH_SHORT).show();

        }else{

            Intent intent = new Intent(MainActivity.this, AppActivity.class);
            intent.putExtra("CLIENTE", c);
            startActivity(intent);

        }

    }
}
