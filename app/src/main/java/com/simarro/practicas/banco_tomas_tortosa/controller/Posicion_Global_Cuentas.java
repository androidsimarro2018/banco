package com.simarro.practicas.banco_tomas_tortosa.controller;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.simarro.practicas.banco_tomas_tortosa.R;
import com.simarro.practicas.banco_tomas_tortosa.fragment.Activity_Fragment_Cuenta;
import com.simarro.practicas.banco_tomas_tortosa.fragment.Activity_Fragment_Movimiento;
import com.simarro.practicas.banco_tomas_tortosa.fragment.CuentaListener;
import com.simarro.practicas.banco_tomas_tortosa.pojo.Cliente;
import com.simarro.practicas.banco_tomas_tortosa.pojo.Cuenta;

public class Posicion_Global_Cuentas extends AppCompatActivity implements CuentaListener {

    Activity_Fragment_Cuenta frgCuentas;

    private Cliente cli;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_activity_cuentas);

        Bundle parametros = this.getIntent().getExtras();

        if (parametros != null) {

            cli = (Cliente) this.getIntent().getExtras().get("CLIENTE");

            frgCuentas = (Activity_Fragment_Cuenta) getSupportFragmentManager().findFragmentById(R.id.frgCuenta);
            //frgCuentaTablet = (Activity_Fragment_Cuenta) getSupportFragmentManager().findFragmentById(R.id.frgCuentaTablet);

            frgCuentas.mostrarCuentas(cli);
            frgCuentas.setListaCuentasListener(this);

        }

    }

    public boolean onCreateOptionsMenu(Menu menu) {
// Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        Intent intent = new Intent(this, Construccion.class);

        switch (item.getItemId()) {
            case R.id.posicionGlobal:
                Intent intentPosicion = new Intent(this, Posicion_Global_Cuentas.class);
                Bundle b = new Bundle();
                b.putSerializable("CLIENTE", cli);
                intentPosicion.putExtras(b);
                startActivity(intentPosicion);
                return true;
            case R.id.ingresos:
                startActivity(intent);
                return true;
            case R.id.transferencias:
                Intent intentTransferencia = new Intent(this, Transferencia.class);
                startActivity(intentTransferencia);
                return true;
            case R.id.cambiarContrasenya:
                Intent intentContrasenya = new Intent(this, Cambio_Contrasenya.class);
                intentContrasenya.putExtra("CLIENTE",cli);
                startActivity(intentContrasenya);
                return true;
            case R.id.promociones:
                startActivity(intent);
                return true;
            case R.id.cajerosCercanos:
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    // PREGUNTAR A ANNA QUE HI HA QUE FER PER A CREAR EL FRAGMENT PER A LA TABLET
    @Override
    public void onCuentaSeleccionada(Cuenta cuenta) {

        boolean hayMovimientos = (getSupportFragmentManager().findFragmentById(R.id.frgMovimiento)!=null);

        if(hayMovimientos){ // TABLET

            ((Activity_Fragment_Movimiento)getSupportFragmentManager().findFragmentById(R.id.frgMovimiento)).mostrarMovimientos(cuenta);

        }else{ // MOBIL

            Intent i = new Intent(this, Posicion_Global_Movimientos.class);
            i.putExtra("Cuenta", cuenta);
            startActivity(i);

        }

    }
}
