package com.simarro.practicas.banco_tomas_tortosa.controller;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ArrayAdapter;

import com.simarro.practicas.banco_tomas_tortosa.R;
import com.simarro.practicas.banco_tomas_tortosa.fragment.Activity_Fragment_Cuenta;
import com.simarro.practicas.banco_tomas_tortosa.fragment.Activity_Fragment_Movimiento;
import com.simarro.practicas.banco_tomas_tortosa.fragment.MovimientoListener;
import com.simarro.practicas.banco_tomas_tortosa.pojo.Cliente;
import com.simarro.practicas.banco_tomas_tortosa.pojo.Cuenta;
import com.simarro.practicas.banco_tomas_tortosa.pojo.Movimiento;

import java.util.ArrayList;

public class Posicion_Global_Movimientos extends AppCompatActivity {

    private ArrayAdapter<Movimiento> adapterMovimiento;
    private ArrayList<Movimiento> listaMovimientos = new ArrayList();
    private Cuenta cuenta;
    Activity_Fragment_Movimiento frgMovimiento;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_activity_movimiento);

        Bundle parametros = this.getIntent().getExtras();

        if (parametros != null) {

            cuenta = (Cuenta) this.getIntent().getExtras().get("Cuenta");
            Log.d("MOVIMO", String.valueOf(cuenta.getListaMovimientos().size()));

            frgMovimiento = (Activity_Fragment_Movimiento) getSupportFragmentManager().findFragmentById(R.id.frgMovimiento);

            frgMovimiento.mostrarMovimientos(cuenta);
            //frgMovimiento.setListaMovimientoListener(this);

        }

    }

}
