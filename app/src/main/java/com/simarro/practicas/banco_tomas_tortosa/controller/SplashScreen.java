package com.simarro.practicas.banco_tomas_tortosa.controller;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;

import com.simarro.practicas.banco_tomas_tortosa.R;

import java.util.Locale;

public class SplashScreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                Intent intent = new Intent(SplashScreen.this, MainActivity.class);

                startActivity(intent);
                finish();

            }
        },500);

        setAppLocale();

    }

    private void setAppLocale() {

        SharedPreferences preferencias = PreferenceManager.getDefaultSharedPreferences(this);
        String idioma = preferencias.getString("idioma", "ESP");
        Resources res = SplashScreen.this.getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        android.content.res.Configuration configuracion = res.getConfiguration();
        configuracion.setLocale(new Locale(idioma.toLowerCase()));
        res.updateConfiguration(configuracion,dm);
    }

}
