package com.simarro.practicas.banco_tomas_tortosa.controller;

import android.content.Intent;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.Guideline;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.simarro.practicas.banco_tomas_tortosa.R;
import com.simarro.practicas.banco_tomas_tortosa.bd.MiBD;
import com.simarro.practicas.banco_tomas_tortosa.pojo.Cliente;
import com.simarro.practicas.banco_tomas_tortosa.pojo.Cuenta;
import com.simarro.practicas.banco_tomas_tortosa.pojo.Movimiento;

import java.util.Calendar;
import java.util.Date;

public class Transferencia extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener, RadioGroup.OnCheckedChangeListener, CompoundButton.OnCheckedChangeListener{

    private Spinner spCuentas;
    private Spinner spCuentasPersonales;

    private RadioButton rbPersonal;
    private RadioButton rbAjena;

    private EditText txtCuentaDestino;
    private EditText importe;

    private String[] datos; // ARRAY PER ALS COMPTES
    private String[] datosSpinner = new String[]{"€", "$", "£", "¥"};

    private Spinner spOpciones;

    private ArrayAdapter<String> adapter;
    private ArrayAdapter<String> adapterSpinner;

    private double numAleatorio;
    private String mostrar = "";

    private ConstraintLayout cl;

    private Guideline gl1;
    private Guideline gl2;

    private ConstraintLayout.LayoutParams parametro1;
    private ConstraintLayout.LayoutParams parametro2;

    private RadioGroup rg;

    private CheckBox chbJustificante;

    private Button btnOk;
    private Button btnCancelar;

    private Cliente c;

    private Cuenta cuentaOrigen, cuentaDestino;
    private MiBD mibd;
    private String auxBanco, auxSucursal, auxDc, auxImporte;
    private String auxCuentaDestino, auxCuentaOrigen;
    private Movimiento m;
    private Float auxSaldo = 1000f;
    private Cuenta cOrigen, cDestino;
    Calendar calendar = Calendar.getInstance();
    Date date =  calendar.getTime();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transferencia);

        Bundle parametros = this.getIntent().getExtras();

        if (parametros != null) {

            c = (Cliente) this.getIntent().getExtras().get("CLIENTE");

        }

        mibd = MiBD.getInstance(this);
        auxBanco= "1001";
        auxSucursal="1001";
        auxDc="11";

        // Declarem variables per a donar-li un tamany als guidelines i controlar la posició

        // ************************ //
        cl = findViewById(R.id.layoutOcultar);
        gl1 = findViewById(R.id.guideline);
        parametro1 = (ConstraintLayout.LayoutParams) gl1.getLayoutParams();
        parametro1.guidePercent = 0.3f;
        gl1.setLayoutParams(parametro1);

        gl2 = findViewById(R.id.horizontalSext);
        parametro2 = (ConstraintLayout.LayoutParams) gl2.getLayoutParams();
        parametro2.guidePercent = 0.5f;
        gl2.setLayoutParams(parametro2);

        // ************************ //

        rg = findViewById(R.id.radioGroup);

        importe = findViewById(R.id.txtImporte);

        chbJustificante = findViewById(R.id.chbJustificante);

        spCuentas = findViewById(R.id.spinnerCuentas);
        spCuentasPersonales = findViewById(R.id.spCuentasPersonales);

        rbPersonal = findViewById(R.id.rbPersonal);
        rbAjena = findViewById(R.id.rbAjena);

        txtCuentaDestino = findViewById(R.id.txtCuentaDestino);

        spOpciones =findViewById(R.id.spOpciones);

        datos = new String[c.getListaCuentas().size()];

        for (int i=0;i<=c.getListaCuentas().size()-1;i++){
            datos[i] = c.getListaCuentas().get(i).getNumeroCuenta();
          //  datos[i-1] = String.valueOf(i) + String.valueOf(i) + String.valueOf(i) + String.valueOf(i) + String.valueOf(i);
        }

        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, datos);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spCuentas.setAdapter(adapter);
        spCuentasPersonales.setAdapter(adapter);

        adapterSpinner = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, datosSpinner);
        adapterSpinner.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spOpciones.setAdapter(adapterSpinner);

        btnOk = findViewById(R.id.btnOk);
        btnCancelar = findViewById(R.id.btnCancelar);

        btnOk.setOnClickListener(this);
        btnCancelar.setOnClickListener(this);

        rbPersonal.setOnClickListener(this);
        rbAjena.setOnClickListener(this);
        rg.setOnCheckedChangeListener(this);
        chbJustificante.setOnCheckedChangeListener(this);

    }

    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        Intent intent = new Intent(this, Construccion.class);

        switch (item.getItemId()) {
            case R.id.posicionGlobal:
                Intent intentPosicion = new Intent(this, Posicion_Global_Cuentas.class);
                Bundle b = new Bundle();
                b.putSerializable("CLIENTE", c);
                intentPosicion.putExtras(b);
                startActivity(intentPosicion);
                return true;
            case R.id.ingresos:
                startActivity(intent);
                return true;
            case R.id.transferencias:
                Intent intentTransferencia = new Intent(this, Transferencia.class);
                startActivity(intentTransferencia);
                return true;
            case R.id.cambiarContrasenya:
                Intent intentContrasenya = new Intent(this, Cambio_Contrasenya.class);
                intentContrasenya.putExtra("CLIENTE",c);
                startActivity(intentContrasenya);
                return true;
            case R.id.promociones:
                startActivity(intent);
                return true;
            case R.id.cajerosCercanos:
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.btnOk:

                mostrar += "Cuenta Origen: " + spCuentas.getSelectedItem().toString() + "\n";

                if(txtCuentaDestino.getText().toString().equals("")){

                    mostrar += "Cuenta Destino: " + spCuentasPersonales.getSelectedItem().toString() + "\n";

                    auxCuentaOrigen = spCuentas.getSelectedItem().toString();
                    auxCuentaDestino = spCuentasPersonales.getSelectedItem().toString();

                    // Igualem conters a valors

                    for (Cuenta c : c.getListaCuentas()){

                        if(auxCuentaOrigen.equals(c.getNumeroCuenta())){

                            cuentaOrigen = c;

                        }

                    }

                    for (Cuenta c : c.getListaCuentas()){

                        if(auxCuentaDestino.equals(c.getNumeroCuenta())){

                            cuentaDestino = c;

                        }

                    }

                    auxImporte = importe.getText().toString();

                    m = new Movimiento(1, 0, date, "Recibo", Float.valueOf(auxImporte), cuentaOrigen, cuentaDestino);

                    mibd.insercionMovimiento(m);

                    mibd.actualizarSaldo(cuentaDestino);
                    mibd.actualizarSaldo(cuentaOrigen);

                }else{

                    mostrar += "Cuenta Destino: " + txtCuentaDestino.getText().toString() + "\n";

                    auxCuentaOrigen = spCuentas.getSelectedItem().toString();
                    auxCuentaDestino = txtCuentaDestino.getText().toString();

                    //Comprovem que el conte destino existeix

                    if(mibd.existeCuenta(auxBanco, auxSucursal, auxDc, auxCuentaDestino)){ // Si la cuenta existe

                        //m = new Movimiento(1, 0, date, "Recibo", importe.getText().toString(), auxCuentaOrigen, auxCuentaDestino);

                    }else { // Si la cuenta No existe

                        Toast.makeText(this, "La cuenta Destino introducido NO existe", Toast.LENGTH_SHORT);

                    }
                }

                mostrar += "Importe introducido: " + importe.getText() + "\n";

                Toast.makeText(this, mostrar, Toast.LENGTH_LONG).show();



                break;

            case R.id.btnCancelar:

                mostrar = "";
                importe.setText("");

                break;

        }

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        mostrar += "Moneda seleccionada: " + parent.getItemAtPosition(position) + "\n";

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    // En este método mirem quin radiobutton està seleccionat per a adaptar els guideline a la pantalla
    // d'aquesta manera podem fer que estiguen un damunt de l'altre, sense ocupar molt d'espai

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {

        if(checkedId == R.id.rbPersonal){

            cl.setVisibility(View.VISIBLE);

            spCuentasPersonales.setVisibility(View.VISIBLE);
            txtCuentaDestino.setVisibility(View.INVISIBLE);

            parametro1.guidePercent = 0.3f;
            gl1.setLayoutParams(parametro1);

            parametro2.guidePercent = 0.5f;
            gl2.setLayoutParams(parametro2);

        }else if(checkedId == R.id.rbAjena){

            cl.setVisibility(View.VISIBLE);

            spCuentasPersonales.setVisibility(View.INVISIBLE);
            txtCuentaDestino.setVisibility(View.VISIBLE);

            parametro1.guidePercent = 0.3f;
            gl1.setLayoutParams(parametro1);

            parametro2.guidePercent = 0.5f;
            gl2.setLayoutParams(parametro2);

        }

    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

        if(isChecked){

            mostrar += "Se quiere enviar Justificante" + "\n";

        }

    }

}
