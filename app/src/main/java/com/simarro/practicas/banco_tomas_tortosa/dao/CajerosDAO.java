package com.simarro.practicas.banco_tomas_tortosa.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.simarro.practicas.banco_tomas_tortosa.bd.MiBD;
import com.simarro.practicas.banco_tomas_tortosa.pojo.Cajeros;

import java.util.ArrayList;

public class CajerosDAO implements PojoDAO{

//*********
//* ESTRUCTURA DE LA TABLA CAJEROS
//*********
    /** Campo identificador de la tabla cajeros */
    public static final String FIELD_CAJEROS_ID = "_id";
    /** Campo que describe la direccion donde se encuentra */
    public static final String FIELD_DIRECCION = "direccion";
    /** Campo que almacena la latitud. */
    public static final String FIELD_LAT = "latitude";
    /** Campo que almacena la longitud */
    public static final String FIELD_LNG = "longitude";
    /** Campo que almacena el nivel del zoom del mapa */
    public static final String FIELD_ZOOM = "zoom";
    /** Almacena el nombre de la tabla */
    public static final String CAJEROS_TABLE = "cajeros";
    /** Array de campos de tabla de cajeros */
    public static final String[] CAMPOS_CAJEROS = new String[]{FIELD_CAJEROS_ID,FIELD_DIRECCION,FIELD_LAT,FIELD_LNG,FIELD_ZOOM};

    private Context contexto;
    private MiBD miBD;
    private SQLiteDatabase db;

    public CajerosDAO() {
    }

    public CajerosDAO abrir(){
        miBD = MiBD.getInstance(contexto);
        db = miBD.getWritableDatabase();
        return this;
    }

    public void cerrar() {
        miBD.close();
    }

    public Cursor getCursor() {
        Cursor c = db.query( true, CAJEROS_TABLE, CAMPOS_CAJEROS, null, null, null, null, null, null);
        c.moveToFirst(); // esta línea es necesaria per a que funcione
        return c;
    }

    public long add(Object obj){

        ContentValues contentValues = new ContentValues();
        Cajeros c = (Cajeros) obj;
        contentValues.put("direccion", c.getDireccion());
        contentValues.put("latitude", c.getLatitude());
        contentValues.put("longitude", c.getLongitude());
        contentValues.put("zoom",c.getDireccion());

        return MiBD.getDB().insert("cajeros", null, contentValues);

    }

    @Override
    public int update(Object obj) {
        return 0;
    }

    @Override
    public void delete(Object obj) {

    }

    @Override
    public Object search(Object obj) {
        return null;
    }

    @Override
    public ArrayList getAll() {
        ArrayList<Cajeros> listaCajeros = new ArrayList<Cajeros>();
        String[] columnas = {"_id","direccion","latitude","longitude","zoom"
        };
        Cursor cursor = MiBD.getDB().query("cajeros", columnas, null, null, null, null, null);

        if (cursor.moveToFirst()) {
            //Recorremos el cursor hasta que no haya más registros
            do {
                Cajeros c = new Cajeros();
                c.setId(cursor.getInt(0));
                c.setDireccion(cursor.getString(1));
                c.setLatitude(cursor.getString(2));
                c.setLongitude(cursor.getString(3));
                c.setZoom(cursor.getString(4));

                listaCajeros.add(c);

            } while(cursor.moveToNext());
        }
        cursor.close();
        return listaCajeros;
    }

}
