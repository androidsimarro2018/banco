package com.simarro.practicas.banco_tomas_tortosa.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.simarro.practicas.banco_tomas_tortosa.R;
import com.simarro.practicas.banco_tomas_tortosa.adaptador.adaptadorCuentas;
import com.simarro.practicas.banco_tomas_tortosa.pojo.Cliente;
import com.simarro.practicas.banco_tomas_tortosa.pojo.Cuenta;

import java.util.ArrayList;

public class Activity_Fragment_Cuenta extends Fragment implements AdapterView.OnItemClickListener {

    private ListView lvCuentas;
    private ArrayList<Cuenta> listaCuentas;
    private adaptadorCuentas adapter;
    private CuentaListener listener;
    private TextView txtNom;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){

        return inflater.inflate(R.layout.layout_fragment_cuentas, container, false);

    }

    public void setListaCuentasListener(CuentaListener listener){

        this.listener = listener;

    }

    public void mostrarCuentas(Cliente cliente){

        txtNom = getView().findViewById(R.id.txtCuentas);
        txtNom.setText(cliente.getNombre());

        lvCuentas = getView().findViewById(R.id.lvCuentas);
        //Log.d("CUENTAS", String.valueOf( cliente.getListaCuentas().size()));
        adapter = new adaptadorCuentas(this, cliente.getListaCuentas());

        lvCuentas.setAdapter(adapter);
        lvCuentas.setOnItemClickListener(this);

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        if(listener!=null){

            listener.onCuentaSeleccionada((Cuenta) lvCuentas.getAdapter().getItem(position));

        }

    }

}
