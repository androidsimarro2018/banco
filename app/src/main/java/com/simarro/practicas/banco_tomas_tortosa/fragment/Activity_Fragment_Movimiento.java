package com.simarro.practicas.banco_tomas_tortosa.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.simarro.practicas.banco_tomas_tortosa.R;
import com.simarro.practicas.banco_tomas_tortosa.adaptador.adaptadorCuentas;
import com.simarro.practicas.banco_tomas_tortosa.adaptador.adaptadorMovimientos;
import com.simarro.practicas.banco_tomas_tortosa.pojo.Cliente;
import com.simarro.practicas.banco_tomas_tortosa.pojo.Cuenta;
import com.simarro.practicas.banco_tomas_tortosa.pojo.Movimiento;

public class Activity_Fragment_Movimiento extends Fragment implements AdapterView.OnItemClickListener {

    private ListView lvMovimientos;
    private TextView textTitulo;
    private Adapter adapter;
    private MovimientoListener listener;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){

        return inflater.inflate(R.layout.layout_fragment_movimiento, container, false);

    }

    public void setListaMovimientoListener(MovimientoListener listener){

        this.listener = listener;

    }

    public void mostrarMovimientos(Cuenta cuenta){

        lvMovimientos = getView().findViewById(R.id.listViewMovimientos);
        //TEXTVIEW per a mostrar moviments
        textTitulo = getView().findViewById(R.id.textView2);
        textTitulo.setText(String.valueOf(cuenta.getId()));

        adapter = new adaptadorMovimientos(this, cuenta.getListaMovimientos());

        lvMovimientos.setAdapter((ListAdapter) adapter);
        lvMovimientos.setOnItemClickListener(this);

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        FragmentManager fragmentManager = this.getActivity().getSupportFragmentManager();
        DialogoPersonalizado dialogoPersonalizado = DialogoPersonalizado.newInstance((Movimiento) parent.getItemAtPosition(position));
        dialogoPersonalizado.show(fragmentManager, "tagMovimiento");

        //if(listener!=null){

            //listener.onMovimientoSeleccionado((Movimiento) lvMovimientos.getAdapter().getItem(position));

        //}

    }
}
