package com.simarro.practicas.banco_tomas_tortosa.fragment;

import com.simarro.practicas.banco_tomas_tortosa.pojo.Cuenta;

public interface CuentaListener {

        void onCuentaSeleccionada(Cuenta c);

}
