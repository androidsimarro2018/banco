package com.simarro.practicas.banco_tomas_tortosa.fragment;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.widget.TextView;

import com.simarro.practicas.banco_tomas_tortosa.R;
import com.simarro.practicas.banco_tomas_tortosa.pojo.Movimiento;

public class DialogoPersonalizado extends DialogFragment {

    TextView txtId;
    TextView txtDescripcion;
    TextView txtImporte;
    TextView txtTipo;
    TextView txtOrigen;
    TextView txtDestino;

    static DialogoPersonalizado newInstance(Movimiento mov) {

        DialogoPersonalizado dialogoPersonalizado = new DialogoPersonalizado();
        Bundle bundle = new Bundle();
        bundle.putSerializable("mov",mov);
        dialogoPersonalizado.setArguments(bundle);

        return dialogoPersonalizado;
    }

    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View view = getActivity().getLayoutInflater().inflate(R.layout.dialog_personal, null);
        Movimiento m = (Movimiento) getArguments().getSerializable("mov");

        builder.setView(view);

        txtId = view.findViewById(R.id.txtId);
        txtDescripcion = view.findViewById(R.id.txtDescripcion);
        txtImporte = view.findViewById(R.id.txtImporte);
        txtTipo = view.findViewById(R.id.txtTipo);
        txtOrigen = view.findViewById(R.id.txtOrigen);
        txtDestino = view.findViewById(R.id.txtDestino);

        txtId.setText("ID: " + m.getId());
        txtDescripcion.setText("Descripción: " + m.getDescripcion());
        txtId.setText("ID: " + m.getId());
        txtDescripcion.setText("Descripción: " + m.getDescripcion());
        txtImporte.setText("Importe: " + m.getImporte());
        txtTipo.setText("Tipo: " + m.getTipo());
        txtOrigen.setText("Cuenta Origen: " + m.getCuentaOrigen().getNumeroCuenta());
        txtDestino.setText("Cuenta Destino: " + m.getCuentaDestino().getNumeroCuenta());

        return builder.create();

    }

    //public void onClick(View v) {
    //getDialog().cancel();
    //}


}
