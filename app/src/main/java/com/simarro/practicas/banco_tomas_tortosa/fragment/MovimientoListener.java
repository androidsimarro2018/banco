package com.simarro.practicas.banco_tomas_tortosa.fragment;

import com.simarro.practicas.banco_tomas_tortosa.pojo.Cuenta;
import com.simarro.practicas.banco_tomas_tortosa.pojo.Movimiento;

public interface MovimientoListener {

    void onMovimientoSeleccionado(Movimiento m);

}
