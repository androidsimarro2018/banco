package com.simarro.practicas.banco_tomas_tortosa.pojo;

import android.support.v7.app.AppCompatActivity;

public class Cajeros extends AppCompatActivity {

    private int id;
    private String direccion;
    private String latitude;
    private String longitude;
    private String zoom;

    public Cajeros() {
    }

    public Cajeros(int id, String direccion, String latitude, String longitude, String zoom) {
        this.id = id;
        this.direccion = direccion;
        this.latitude = latitude;
        this.longitude = longitude;
        this.zoom = zoom;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getZoom() {
        return zoom;
    }

    public void setZoom(String zoom) {
        this.zoom = zoom;
    }
}
